﻿using System;

namespace Entity
{
    public class User
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Dob { get; set; }
        public string Lead_source { get; set; }
    }
}